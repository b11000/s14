/*
	Activity: Create a function called getBio that accepts 4 arguments.
	lname, fname, email, mobileNum,

	Exceed Output:
	Hello! I am Steve Rogers
	You can contact me capt_am@gmail.com
	Mobile Number:09171234567
	*/

function getBio(lname, fname, email, mobileNum){
	console.log('Hello! I am Captain ' + fname + ' ' + lname + ' ' + email + ' ' + mobileNum)

}

getBio('Rogers', 'Steve', 'capt_am@gmail.com', '09171234567' )